package com.company.repositories;

import com.company.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

/**
 * 20.08.2021
 * 33. Simple Program with JDBC
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Repository
public class ProductsRepositoryNamedParameterJdbcTemplateImpl implements ProductsRepository {

    //language=SQL
    private static final String SQL_INSERT =
            "insert into product(product_name, quantity) values (:product_name, :quantity) RETURNING id";

    //language=SQL
    private static final String SQL_SELECT_ALL =
            "select * from product order by id";

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private static final RowMapper<Product> productRowMapper = (row, rowNumber) -> Product.builder()
            .id(row.getLong("id"))
            .productName(row.getString("product_name"))
            .quantity(row.getInt("quantity"))
            .build();

    @Autowired
    public ProductsRepositoryNamedParameterJdbcTemplateImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public void save(Product product) {
        // данный объект запоминает сгенерированные базой данных ключи
        KeyHolder keyHolder = new GeneratedKeyHolder();
        namedParameterJdbcTemplate.update(SQL_INSERT,
                (new MapSqlParameterSource()
                .addValue("product_name", product.getProductName())
                .addValue("quantity", product.getQuantity())),
                keyHolder, new String[]{"id"});
        product.setId(keyHolder.getKey().longValue());
    }

    @Override
    public List<Product> findAll() {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
    }
}
