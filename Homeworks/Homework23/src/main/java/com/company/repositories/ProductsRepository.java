package com.company.repositories;

import com.company.models.Product;

import java.util.List;

/**
 * 20.08.2021
 * 33. Simple Program with JDBC
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface ProductsRepository {
    void save(Product account);
    List<Product> findAll();
}
