package com.company.services;

import java.util.List;

/**
 * 20.09.2021
 * 41. Java Servlet Example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface UsersService {
    void signUp(String productName, int quantity);

    List<String> getAllProducts();

//    List<String> searchForProducts(String wordForSearch);
}
