package com.company.services;

import com.company.models.Product;
import com.company.repositories.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 06.09.2021
 * 36. Spring Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Service
public class UsersServiceImpl implements UsersService {

    private final ProductsRepository productsRepository;

    @Autowired
    public UsersServiceImpl(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    @Override
    public void signUp(String productName, int quantity) {
        Product product = Product.builder()
                .productName(productName)
                .quantity(quantity)
                .build();

        productsRepository.save(product);
    }

    @Override
    public List<String> getAllProducts() {
        // a -> a.method() -> A::method
        // a -> System.out.println(a) -> System.out::println
        // a -> x.method(a) -> x::method
        return productsRepository.findAll().stream().map(Product::getProductName).collect(Collectors.toList());
    }

//    @Override
//    public List<String> searchForProducts(String wordForSearch) {
//        return productsRepository.findAll().stream().map(Product::getProductName).filter(wordForSearch::equals).collect(Collectors.toList());
//    }
}
