package com.company.servlets;

import com.company.config.ApplicationConfig;
import com.company.services.UsersService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/searchResults")
public class SearchResultsServlet extends HttpServlet {
    private UsersService usersService;
    @Override
    public void init() throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        this.usersService = context.getBean(UsersService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        String wordForSearch = request.getParameter("wordForSearch");
        writer.println("<h1>Products found with a word:</h1>");
        writer.println("<table>");
        writer.println("    <tr>");
        writer.println("        <th>" + "Found products are" + "</th>");
        writer.println("    </tr>");
        for (String productName : usersService.getAllProducts()) {
            if (!(productName ==null)) {
                if (productName.contains(wordForSearch)) {
                    writer.println("<tr>");
                    writer.println("    <td>" + productName + "</td>");
                    writer.println("</tr>");
                }
            }
        }
        writer.println("</table>");
    }
}
