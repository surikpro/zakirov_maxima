package com.company.servlets;

import com.company.config.ApplicationConfig;
import com.company.services.UsersService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/searchProduct")
public class SearchProductServlet extends HttpServlet {
    private UsersService usersService;
    @Override
    public void init() throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        this.usersService = context.getBean(UsersService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        writer.println("<h1>My Products</h1>\n" +
                "<p>Enter the word for search!</p>\n" +
                "<br>\n" +
                "<form action=\"/Web_Project_with_DB_war/searchResults\">\n" +
                "\t<label for=\"search\">Your query:</label>\n" +
                "\t<input id=\"search\" type=\"text\" name=\"wordForSearch\" placeholder=\"Enter text\">\n" +
                "\t<input type=\"submit\" name=\"\" value=\"SEARCH\">\n" +
                "</form>");
    }
}
