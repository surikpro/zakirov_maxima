package com.company.servlets;

import com.company.config.ApplicationConfig;
import com.company.services.UsersService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 20.09.2021
 * 41. Java Servlet Example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@WebServlet("/signUp")
public class SignUpServlet extends HttpServlet {
    private UsersService usersService;

    @Override
    public void init() throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        this.usersService = context.getBean(UsersService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.getWriter().println("<h1>Sign Up the product and its quantity</h1>\n" +
                "<p>Please enter the name and quantity of the product</p>\n" +
                "<br>\n" +
                "<form method=\"post\">\n" +
                "\t<label for=\"productName\">Enter name of the product here:</label>\n" +
                "\t<input id=\"productName\" type=\"text\" name=\"productName\" placeholder=\"Your product name\">\n" +
                "\t<label for=\"quantity\">Enter the quantity here:</label>\n" +
                "\t<input id=\"quantity\" type=\"text\" name=\"quantity\" placeholder=\"Your quantity\">\n" +
                "\t<input type=\"submit\" name=\"\" value=\"Sign Up\">\n" +
                "</form>");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String productName = request.getParameter("productName");
        int quantity = Integer.parseInt(request.getParameter("quantity"));
        usersService.signUp(productName, quantity);
        // отправляем браузеру Header location=/products, и браузер сам перейдет по этому урлу
        response.sendRedirect("/Web_Project_with_DB_war/products");
    }
}
