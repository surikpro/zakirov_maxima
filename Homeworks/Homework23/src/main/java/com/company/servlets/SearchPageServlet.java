package com.company.servlets;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 13.09.2021
 * 41. Java Servlet Example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class SearchPageServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter writer = response.getWriter();
        writer.println("<h1>My Google</h1>\n" +
                "<p>Enter text for search!</p>\n" +
                "<br>\n" +
                "<form action=\"http://google.com/search\">\n" +
                "\t<label for=\"search\">Your query:</label>\n" +
                "\t<input id=\"search\" type=\"text\" name=\"q\" placeholder=\"Enter text\">\n" +
                "\t<input type=\"submit\" name=\"\" value=\"SEARCH\">\n" +
                "</form>");
    }
}
