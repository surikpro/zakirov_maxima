public class GunWeaponGenerator implements IWeaponGenerator {
    @Override
    public IWeapon generateWeapon() {
        return new GunWeapon();
    }
}
