public class Main {
    public static void main(String[] args) {
        Player player = new Player("Aydar", 100);
        player.hit(1);
        System.out.println("************");
        Weapon weapon = new Weapon("axe");
        weapon.hit(2);
        System.out.println("************");
        WeaponProxy weaponAxe = new WeaponProxy(weapon);
        weaponAxe.setDamageValue(10);
        weaponAxe.hit(4);
        System.out.println("************");
        WeaponImpl weaponImpl = new WeaponImpl(weapon);
        System.out.println(weaponImpl);
        weaponImpl.hit(1);
        System.out.println("************");
        IWeaponGenerator weaponGenerator = new AxeWeaponGenerator();
        IWeapon axeWeapon = weaponGenerator.generateWeapon();
        axeWeapon.weaponGenerate();
        IWeaponGenerator gunWeaponGenerator = new GunWeaponGenerator();
        IWeapon gunWeapon = gunWeaponGenerator.generateWeapon();
        gunWeapon.weaponGenerate();
        System.out.println("************");
        GameSite gameSite = new GameSite();
        Player ayrat = new Player("Ayrat", 100);
        Player aynur = new Player("Aynur", 100);
        Player maxim = new Player("Maxim", 100);
        gameSite.addObserver(ayrat);
        gameSite.addObserver(aynur);
        gameSite.addObserver(maxim);
        ayrat.setHealth(0);
        gameSite.setMessage("Игрок ушел из игры");
        System.out.println("************");



    }
}
