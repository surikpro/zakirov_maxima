public class StandardIDamageAlgorithm implements IDamage {
    private IDamage IDamage;
    private Weapon weapon;

    @Override
    public void hit(int hitIndex) {
        System.out.println("You applied standard damage with " + weapon.getWeaponName());
    }
}
