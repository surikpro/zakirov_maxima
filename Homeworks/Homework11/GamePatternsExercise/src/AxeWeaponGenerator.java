public class AxeWeaponGenerator implements IWeaponGenerator {
    @Override
    public IWeapon generateWeapon() {
        return new AxeWeapon();
    }
}
