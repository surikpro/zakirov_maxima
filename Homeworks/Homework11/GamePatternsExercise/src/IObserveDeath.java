public interface IObserveDeath {
    void addObserver(Player player);
    void removeObserver(Player player);
    void notifyObservers();
}
