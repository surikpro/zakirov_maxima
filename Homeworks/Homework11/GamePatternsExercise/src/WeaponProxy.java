public class WeaponProxy implements IDamage {
    private String weaponName;
    private Weapon weapon;
    private int damageValue;

    public WeaponProxy(Weapon weapon) {
        this.weaponName = weapon.getWeaponName();
        this.weapon = weapon;
    }

    public void setDamageValue(int damageValue) {
        this.damageValue = damageValue;
    }

    @Override
    public void hit(int hitIndex) {
        if (weapon == null) {
            System.out.println("Standard damage is " + damageValue);
        } else {
            weapon = new Weapon(weaponName);
            weapon.hit(hitIndex);
            System.out.println("Total damage with proxy " + damageValue * hitIndex);
        }
    }
}
