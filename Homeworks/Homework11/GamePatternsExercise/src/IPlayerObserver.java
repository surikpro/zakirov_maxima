
public interface IPlayerObserver {
    void handleEvent(String message);
}
