public interface IWeaponGenerator {
    IWeapon generateWeapon();
}
