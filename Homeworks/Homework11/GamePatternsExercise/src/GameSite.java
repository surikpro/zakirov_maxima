import java.util.ArrayList;
import java.util.List;

public class GameSite implements IObserveDeath {
    private String message;
    List<Player> players = new ArrayList<>();


    public void setMessage(String message) {
        this.message = message;
        notifyObservers();
    }

    @Override
    public void addObserver(Player player) {
        this.players.add(player);
    }

    @Override
    public void removeObserver(Player player) {
        this.players.remove(player);
    }

    @Override
    public void notifyObservers() {
        for (Player player: players) {
            player.handleEvent(message);
        }
    }
}
