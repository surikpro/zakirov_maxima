public class Weapon implements IDamage {
    private String weaponName;
    private final int DAMAGE_VALUE = 1;

    public Weapon(String weaponName) {
        this.weaponName = weaponName;
    }

    public String getWeaponName() {
        return weaponName;
    }

    @Override
    public void hit(int hitIndex) {
        System.out.println("I am hitting you with " + weaponName + ". Damage * " +  DAMAGE_VALUE * hitIndex);
    }
}
