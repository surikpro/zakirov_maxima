
public class Player implements IDamage, IPlayerObserver {
    private String name;
    private int health;
    private Weapon weapon;

    public Player(String name, int health) {
        this.name = name;
        this.health = health;
    }

    public String getName() {
        return name;
    }

    public int getHealth() {
        return health;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    @Override
    public void hit(int hitIndex) {
        System.out.println("I am just hitting you with my firm fist! Damage * " + hitIndex);
    }


    @Override
    public void handleEvent(String message) {
      if (health == 0) {
            System.out.println("Игрок " + name + " выбыл из игры " + " со здоровьем " + health); }
    }
}
