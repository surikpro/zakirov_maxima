public class WeaponImpl implements IDamage {
    private IDamage IDamage;
    private Weapon weapon;

    public WeaponImpl(Weapon weapon) {

        if (weapon.getWeaponName().equals("axe")) {
            this.IDamage = new AxeIDamageAlgorithm(weapon, 2);
        } else {
            this.IDamage = new StandardIDamageAlgorithm();
        }
    }

    @Override
    public void hit(int hitIndex) {
        System.out.println("Implementing algorithm according to weapon choice. Damage is " + hitIndex);

    }

    @Override
    public String toString() {
        return getClass().getName();
    }

}
