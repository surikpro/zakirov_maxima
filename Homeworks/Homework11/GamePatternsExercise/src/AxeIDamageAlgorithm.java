public class AxeIDamageAlgorithm implements IDamage {
    private Weapon weapon;
    private int hitIndex;

    public AxeIDamageAlgorithm(Weapon weapon, int hitIndex) {
        this.weapon = weapon;
        this.hitIndex = hitIndex;
        weapon.hit(2);
    }


    @Override
    public void hit(int hitIndex) {
        System.out.println("Standard damage is multiplied by " + hitIndex);
    }

}
