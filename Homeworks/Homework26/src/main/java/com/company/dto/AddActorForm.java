package com.company.dto;

import lombok.Data;

/**
 * 27.10.2021
 * 46. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
public class AddActorForm {
    private String firstName;
    private String lastName;
}
