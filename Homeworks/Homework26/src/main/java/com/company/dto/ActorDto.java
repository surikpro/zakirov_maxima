package com.company.dto;

import com.company.models.Actor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 23.09.2021
 * 42. Java Servlet JSP
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ActorDto {
    private Long id;
    private String firstName;
    private String lastName;

    public static ActorDto from(Actor actor) {
        return ActorDto.builder()
                .id(actor.getId())
                .firstName(actor.getFirstName())
                .lastName(actor.getLastName())
                .build();
    }

    public static List<ActorDto> from(List<Actor> actors) {
        return actors.stream().map(ActorDto::from).collect(Collectors.toList());
    }
}
