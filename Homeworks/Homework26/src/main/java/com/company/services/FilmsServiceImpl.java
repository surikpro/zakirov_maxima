package com.company.services;

import com.company.dto.ActorDto;
import com.company.dto.FilmDto;
import com.company.models.Actor;
import com.company.models.Film;
import com.company.repositories.ActorsRepository;
import com.company.repositories.FilmsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.company.dto.FilmDto.from;
import static com.company.dto.ActorDto.from;

@Service
@RequiredArgsConstructor
public class FilmsServiceImpl implements FilmsService {

    private final ActorsRepository actorsRepository;

    private final FilmsRepository filmsRepository;

    @Override
    public void addFilm(FilmDto film) {
        Film newFilm = Film.builder()
                .name(film.getName())
                .build();
        filmsRepository.save(newFilm);
    }
    @Override
    public List<FilmDto> getAllFilms() {
        return from(filmsRepository.findAll());
    }

    @Override
    public List<ActorDto> getActorsByFilm(Long filmId) {
        return from(actorsRepository.findAllByFilm_Id(filmId));
    }

    @Override
    public void addActorToFilm(Long filmId, ActorDto actorForm) {
        Film film = filmsRepository.getById(filmId);
        Actor actor = actorsRepository.getById(actorForm.getId());
        actor.setFilm(film);
        actorsRepository.save(actor);
    }

    @Override
    public List<ActorDto> getActorsWithoutFilm() {
        return from(actorsRepository.findAllByFilmIsNull());
    }

    @Override
    public FilmDto getFilmById(Long filmId) {

        return from(filmsRepository.getById(filmId));
    }
}
