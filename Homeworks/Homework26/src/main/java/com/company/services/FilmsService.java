package com.company.services;

import com.company.dto.ActorDto;
import com.company.dto.FilmDto;
import com.company.models.Film;

import java.util.List;

import static com.company.dto.FilmDto.from;

public interface FilmsService {
    void addFilm(FilmDto film);
    List<FilmDto> getAllFilms();
    List<ActorDto> getActorsByFilm(Long filmId);
    void addActorToFilm(Long filmId, ActorDto actor);
    List<ActorDto> getActorsWithoutFilm();

    FilmDto getFilmById(Long filmId);
}
