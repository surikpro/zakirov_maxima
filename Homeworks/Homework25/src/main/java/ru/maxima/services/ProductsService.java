package ru.maxima.services;

import ru.maxima.dto.ProductDto;

import java.util.List;

public interface ProductsService {
//    void signUp(String product);
//
//    List<String> getAllNames();
    List<ProductDto> getAllProducts();

//    List<ProductDto> searchProductByName(String name);
}
