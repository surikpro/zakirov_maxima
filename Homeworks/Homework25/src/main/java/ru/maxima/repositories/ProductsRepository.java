package ru.maxima.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.maxima.models.Product;

import java.util.List;
import java.util.Optional;

public interface ProductsRepository extends JpaRepository<Product, Long> {
//    Optional<Product> save(Product product);

    List<Product> findAll();

//    List<Product> searchByName(String name);
}
