package ru.maxima.contollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.maxima.services.ProductsService;

@Controller
public class ProductsController {

    @Autowired
    private ProductsService productsService;

    @RequestMapping("/products")
    public String getAllProducts(Model model) {
        model.addAttribute("products", productsService.getAllProducts());
        return "products_page";
    }
}
