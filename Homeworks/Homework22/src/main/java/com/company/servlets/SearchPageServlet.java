package com.company.servlets;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class SearchPageServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html; charset=UTF-8");
        PrintWriter writer = response.getWriter();
        writer.println("<html>" +
                "<body>\n" +
                "  <h1>Google</h1>\n" +
                "<p>Введите текст для поиска</p>\n" +
                "<br>\n" +
                "<form action=\"http://google.com/search\">\n" +
                "\t<label for=\"search\">Тут надо вводить запрос:</label>\n" +
                "\t<input id=\"search\" type=\"text\" name=\"q\" placeholder=\"Введите поисковый запрос\">\n" +
                "\t<input type=\"submit\" name=\"\" value=\"ПОИСК\">\n" +
                "</form>" +
                "</body>\n" +
                "</html>");
    }
}
