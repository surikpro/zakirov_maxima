import com.zaxxer.hikari.HikariDataSource;
import dto.UserDto;
import models.User;
import repositories.UsersRepository;
import repositories.UsersRepositoryDBBasedImpl;
import services.UsersService;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class MainWithDB {
    public static void main(String[] args) {
        Optional<User> currentOptional = Optional.empty();
        Scanner scanner = new Scanner(System.in);
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setJdbcUrl("jdbc:postgresql://localhost:5432/maxima_DB");
        dataSource.setUsername("postgres");
        dataSource.setPassword("qwerty007");
        dataSource.setMaximumPoolSize(20);

        UsersRepository usersRepository = new UsersRepositoryDBBasedImpl(dataSource);
        UsersService usersService = new UsersService(usersRepository);

        System.out.println("We are finding user by email in DB-based repository: ");
        Optional<User> userOptional0 = usersRepository.findByEmail("gazizov@gmail.com");
        if (userOptional0.isPresent()) {
            System.out.println(userOptional0.get());
        } else {
            System.out.println("Такого пользовтаеля не существует");
        }

        while (true) {
            System.out.println("1. Регистрация");
            System.out.println("2. Аутентификация");
            System.out.println("3. Список пользователей");
            System.out.println("4. Изменить пароль");

            int command = scanner.nextInt();
            scanner.nextLine();

            if (command == 1) {
                String email = scanner.nextLine();
                String password = scanner.nextLine();
                usersService.signUp(email, password);
            } else if (command == 2) {
                String email = scanner.nextLine();
                String password = scanner.nextLine();
                currentOptional = usersService.signIn(email, password);
                if (currentOptional.isPresent()) {
                    System.out.println("Пользователь аутентифицирован");
                } else {
                    System.err.println("Ошибка аутентификации");
                }

            } else if (command == 3) {
                if (!currentOptional.isPresent()) {
                    System.err.println("Вы не прошли аутентификацию");
                    continue;
                }
                List<UserDto> users = usersService.getUsers(currentOptional.get());
                System.out.println(users);
            } else if (command == 4) {
                if (currentOptional.isPresent()) {
                    System.out.println("Введите ваш новый email и пароль: ");
                    String email = scanner.nextLine();
                    String password = scanner.nextLine();
                    User user  = currentOptional.get();
                    user.setEmail(email);
                    user.setPassword(password);
                    usersRepository.update(user);
                } else {
                    System.out.println("Такой пользователь не найден");
                    System.out.println("Пожалуйста, пройдите аутентификацию");
                }
            }
        }
    }
}
