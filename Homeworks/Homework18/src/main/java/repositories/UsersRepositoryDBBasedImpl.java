package repositories;

import models.User;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class UsersRepositoryDBBasedImpl implements UsersRepository{
    //language=SQL
    private static final String SQL_UPDATE_BY_ID =
            "update user_email_credentials set email = ?, password = ? where userId = ?;";
    //language=SQL
    private static final String SQL_SELECT_ALL =
            "select * from user_email_credentials where email = ?";
    //language=SQL
    private static final String SQL_INSERT =
            "insert into user_email_credentials(email, password) values (?, ?)";
    //language=SQL
    private static final String SQL_SELECT_ALL_BY_ID =
            "select * from user_email_credentials order by userId";

    DataSource dataSource;

    public UsersRepositoryDBBasedImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    private final Function<ResultSet, User> rowToAccountMapper = row -> {
        try {
            return new User(
                    row.getInt("userId"),
                    row.getString("email"),
                    row.getString("password")
            );
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    };

    @Override
    public void save(User user) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);) {
            // RETURN_GENERATED_KEYS - флаг, который говорит
            // что из запроса нужно вытащить значения, которые сгенерировала БД
            statement.setString(1, user.getEmail());
            statement.setString(2, user.getPassword());

            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Can't insert");
            }
            // получили сгенерированные ключи
            ResultSet generatedKeys = statement.getGeneratedKeys();
            // если вы не можете получить сгенерированные ключи (которых нет)
            if (!generatedKeys.next()) {
                throw new SQLException("Can't retrieve generated keys");
            }
            // положили в модель сгенерированный базой id
            user.setId(generatedKeys.getInt("userId"));

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Optional<User> findByEmail(String email) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ALL);) {
             preparedStatement.setString(1, email);
             try (ResultSet result = preparedStatement.executeQuery();) {
                 while (result.next()) {
                     if (result.getString("email").equals(email)) {
                         User user = new User(
                                 result.getInt("userId"),
                                 result.getString("email"),
                                 result.getString("password")
                         );
                         return Optional.of(user);
                     }
                     return Optional.empty();
                 }
             }
//            if (!result.next()) {
//                return Optional.empty();
//            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }


        return Optional.empty();
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement();){
             try (ResultSet result = statement.executeQuery(SQL_SELECT_ALL_BY_ID)) {
                 while (result.next()) {
                     User user = rowToAccountMapper.apply(result);
                     users.add(user);
                 }
             }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

        return users;
    }

    @Override
    public void update(User user) {
        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_BY_ID);) {
            statement.setString(1, user.getEmail());
            statement.setString(2, user.getPassword());
            statement.setInt(3, user.getId());
            int affectedRows = statement.executeUpdate();
                if (affectedRows == 0) {
                    throw new SQLException("Can't update");
                }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
