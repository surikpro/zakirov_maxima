import com.zaxxer.hikari.HikariDataSource;
import dto.UserDto;
import models.User;
import repositories.UsersRepository;
import repositories.UsersRepositoryDBBasedImpl;

import services.UsersService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class MainWithDB {
    public static void main(String[] args) {
//        IdGenerator usersIdGenerator = IdGenerators.fileBasedGenerator("users_sequence.txt");
        Optional<User> currentOptional = Optional.empty();
        Scanner scanner = new Scanner(System.in);
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setJdbcUrl("jdbc:postgresql://localhost:5432/maxima_DB");
        dataSource.setUsername("postgres");
        dataSource.setPassword("qwerty007");
        dataSource.setMaximumPoolSize(20);

        UsersRepository usersRepository = new UsersRepositoryDBBasedImpl(dataSource);
        UsersService usersService = new UsersService(usersRepository);

//        UsersRepository usersRepository = new UsersRepositoryFileBasedImpl("users.txt", usersIdGenerator);
//        UsersRepository usersRepository = new UsersRepositoryListImpl();

        while (true) {
            System.out.println("1. Регистрация");
            System.out.println("2. Аутентификация");
            System.out.println("3. Список пользователей");
            System.out.println("4. Изменить пароль");
            System.out.println("5. Поиск по email в БД");

            int command = scanner.nextInt();
            scanner.nextLine();

            if (command == 1) {
                String email = scanner.nextLine();
                String password = scanner.nextLine();
                usersService.signUp(email, password);
            } else if (command == 2) {
                String email = scanner.nextLine();
                String password = scanner.nextLine();
                currentOptional = usersService.signIn(email, password);

                if (!currentOptional.isPresent()) {
                    System.err.println("Ошибка аутентификации");
                }
            } else if (command == 3) {
                if (!currentOptional.isPresent()) {
                    System.err.println("Вы не прошли аутентификацию");
                    continue;
                }
                List<UserDto> users = usersService.getUsers(currentOptional.get());
                System.out.println(users);
            } else if (command == 4) {
                if (currentOptional.isPresent()) {
                    System.out.println("Введите ваш новый email и пароль: ");
                    String email = scanner.nextLine();
                    String password = scanner.nextLine();
                    User user = currentOptional.get();
                    user.setEmail(email);
                    user.setPassword(password);
                    usersRepository.update(user);
                } else {
                    System.out.println("Такой пользователь не найден");
                    System.out.println("Пожалуйста, пройдите аутентификацию");
                }

            } else if (command == 5) {
                if (currentOptional.isPresent()) {
                    System.out.println("Пользователь аутентифицирован");
                    System.out.println("Введите email для поиска: ");
                    String emailForSearch = scanner.nextLine();
                    Optional<User> emailSearchUserOptional = usersService.findByEmail(emailForSearch);
                    if (emailSearchUserOptional.isPresent()) {
                        System.out.println("Email " + emailSearchUserOptional.get().getEmail() + " существует в БД");
                    } else {
                        System.out.println("Такого пользователя не существует");
                    }
                } else {
                    System.err.println("Пожалуйста, пройдите аутентификацию");
                }
            }
        }
    }
}
