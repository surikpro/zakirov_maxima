package solution;

import solution.framework.DocumentsFramework;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        DocumentsFramework framework = new DocumentsFramework();
//        Statement statement = framework.generate(Statement.class, "Сидиков Марсель",
//                LocalDate.of(1994, 2, 2));
//
//        Statement statement1 = framework.generate(Statement.class, "Сидиков Марсель");
////
//        Letter letter = framework.generate(Letter.class, "Марсель", "Айрат");
//        System.out.println(statement);
//        System.out.println(statement1);
//        System.out.println(letter);
        Certificate certificate0 = framework.generate(Certificate.class, 12);
        Certificate certificate1 = framework.generate(Certificate.class, 1);
        Certificate certificate2 = framework.generate(Certificate.class, 5);
        System.out.println(certificate0);
        System.out.println(certificate1);
        System.out.println(certificate2);

    }
}
