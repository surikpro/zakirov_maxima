package solution;

import solution.framework.CheckRange;
import solution.framework.Document;


public class Certificate implements Document {
    @CheckRange(min = 0, max = 10)
    private int activeYears;


    public Certificate(int activeYears) {
        this.activeYears = activeYears;
    }

    @Override
    public String toString() {
        return "Certificate{" +
                "activeYears=" + activeYears +
                '}';
    }
}
