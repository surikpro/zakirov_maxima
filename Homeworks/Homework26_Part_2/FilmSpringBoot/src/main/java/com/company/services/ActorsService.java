package com.company.services;

import com.company.dto.ActorDto;
import com.company.dto.AddActorForm;

import java.util.List;

/**
 * 27.10.2021
 * 46. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface ActorsService {
    void addActor(AddActorForm form);
    List<ActorDto> getAllActors();
}
