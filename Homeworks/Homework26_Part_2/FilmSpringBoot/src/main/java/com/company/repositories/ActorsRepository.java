package com.company.repositories;

import com.company.models.Actor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 18.10.2021
 * 42. Java Servlet JSP
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

public interface ActorsRepository extends JpaRepository<Actor, Long> {
    List<Actor> findAllByFilm_Id(Long filmId);
    List<Actor> findAll();
    List<Actor> findAllByFilmIsNull();

}
