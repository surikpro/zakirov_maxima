package com.company.controllers;

import com.company.dto.AddActorForm;
import com.company.services.ActorsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * 27.10.2021
 * 46. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Controller
public class ActorsController {

    @Autowired
    private ActorsService actorsService;

    @RequestMapping("/actors")
    public String getActorsPage(Model model) {
        model.addAttribute("actors", actorsService.getAllActors());
        return "actors";
    }

    @RequestMapping(value = "/actors", method = RequestMethod.POST)
    public String addActor(AddActorForm form) {
        actorsService.addActor(form);
        return "redirect:/actors";
    }
}
