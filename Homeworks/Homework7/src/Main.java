public class Main {

    public static void main(String[] args) {
        ArrayList list = new ArrayList();
        list.add(10);
        list.add(15);
        list.add(20);
        list.add(25);
        list.add(15);
        list.add(25);

        list.addToBegin(5);
        System.out.print("We are searching item's index by its value. Index of 20 is 2: ");
        System.out.println(list.indexOf(20));
        System.out.print("We are searching last item's index by its value. Index of element 15 is: ");
        System.out.println(list.lastIndexOf(15));
        System.out.print("After removing item by index: ");
        list.remove(4);
        System.out.print("After removing a series of elements by element's value: ");
        list.removeAll(25);
    }
}