package ru.maxima;

public class Main {

    public static void main(String[] args) {
        System.out.println("ArrayList Block");
        List arrayList = new ArrayList();
        arrayList.add("apple");
        arrayList.add("orange");
        arrayList.add("melon");
        arrayList.add("apple");
        arrayList.add("peach");
        System.out.println(arrayList.lastIndexOf("apple"));
        System.out.println(arrayList.lastIndexOf("melon"));
        System.out.println(arrayList.contains("orange"));
        System.out.println(arrayList.contains("watermelon"));
//        arrayList.remove("melon");
        arrayList.remove("peach");
        System.out.println("***************");


        System.out.println("BadMap Block");
        Map mapEntries = new BadMap();
        mapEntries.put("A", "1");
        mapEntries.put("B", "2");
        mapEntries.put("C", "3");
        mapEntries.put("D", "4");
        Set keySet = mapEntries.keySet();
        System.out.println(mapEntries.keySet());
        System.out.println(keySet.isEmpty());
        System.out.println("KeySet size is: " + keySet.size());
        System.out.println(mapEntries.values());
        Collection collection = mapEntries.values();
        System.out.println(collection.contains("1"));
        System.out.println("EntrySet : " + mapEntries.entrySet().size());
        System.out.println("BadMap contains value: " + mapEntries.containsValue("1"));
        System.out.println("***************");

        System.out.println("BadSet Block");
        Set set = new BadSet();
        set.add("D");
        set.add("E");
        set.add("F");
        set.add("G");
        set.remove("D");
        System.out.println("Set contains " + set.contains("D"));
        System.out.println("Set size is: " + set.size());
        System.out.println(set.iterator());
        System.out.println("***************");

        System.out.println("LinkedList Block");
        LinkedList linkedList = new LinkedList();
        linkedList.add("Avril");
        linkedList.add("Taylor");
        linkedList.add("Justin");
        linkedList.add("Jessica");
        linkedList.add("Justin");
        linkedList.add("Bieber");
        System.out.println(linkedList);
        System.out.println(linkedList.indexOf("Avril"));
        System.out.println("LastIndex of element is " + linkedList.lastIndexOf("Justin"));
        System.out.println(linkedList.contains("Bieber"));
        System.out.println(linkedList.contains("Ayrat"));
        linkedList.remove("Taylor");
        System.out.println(linkedList.contains("Taylor"));
        System.out.println("***************");
        System.out.println(linkedList.iterator().hasNext());
        System.out.println("***************");

        System.out.println("HashMap Block");
        Map map = new HashMap();
        map.put("Сидиков", "Марсель");
        map.put("Поздеев", "Максим"); // OK
        map.put("Вдовинов", "Даниил"); // OK
        map.put("Евлампьев", "Виктор"); // OK
        map.put("Мухутдинов", "Айрат"); // OK
        map.put("Мухутдинова", "Алия"); // OK
        map.put("Набиев", "Азат"); // OK
        map.put("Миниахметов", "Разиль"); // OK
        map.put("Хафизов", "Тимур"); // OK
        map.put("Пашаев", "Эмиль"); // OK
        map.put("Батршин", "Тимур"); // OK
        map.put("Беспалов", "Вадим"); // OK
        map.put("Сабирзянова", "Аделя"); // OK
        map.put("Хасанов", "Ильгам"); // OK
        map.put("Забиров", "Салават"); // OK
        map.put("Путин", "Владимир"); // OK
        map.put("Медведев", "Дмитрий"); // OK
        map.put("Метшин", "Ильсур"); // OK
        map.put("Махмутов", "Рафаэль"); // OK

        System.out.println(map.get("Метшин"));
        System.out.println(map.get("Сидиков")); // Рафаэль
        System.out.println(map.containsKey("Хасанов"));
        System.out.println(map.containsValue("Ильсур"));
        System.out.println(map.entrySet().size());
        System.out.println(map.keySet().size());
        System.out.println(map.values().size());
        System.out.println("***************");

    }
}
