package ru.maxima;

/**
 * 08.07.2021
 * 22. Custom Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 *
 * Объекты данного интерфейса гарантируют, что все элементы в них встречаются только один раз
 */
public interface Set<T> extends Collection<T> {
}
