package ru.maxima;

/**
 * 08.07.2021
 * 22. Custom Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class LinkedList<T> implements List<T> {

    private static class Node {
        Object value;
        Node next;

        public Node(Object value) {
            this.value = value;
        }
    }
    // ссылка на первый элемент
    private Node first;
    // ссылка на последний элемент
    private Node last;

    private int size;

    @Override
    public T get(int index) {
        if (indexInBounds(index)) {
            // начинаем с первого узла
            Node current = first;
            for (int i = 0; i < index; i++) {
                // сдвигаем current на нужную позицию
                current = current.next;
            }
            return (T) current.value;
        }
        return null;
    }

    @Override
    public int indexOf(T element) {
        // TODO: реализовать
        Node current = first;

        for (int i = 0; i < size; i++) {
            if (current.value.equals(element)) {
                return i;
            } else {
                current = current.next;
            }
        }
        return 0;
    }

    @Override
    public int lastIndexOf(T element) {
        // TODO: реализовать
        Node current = first;
        int lastIndex = 0;
        int i = 0;
        while (current != null) {
            i++;
            if (current.value.equals(element)) {
                lastIndex = i;
            }
            current = current.next;
        }
        return lastIndex;
    }

    @Override
    public void add(T element) {
        // создали узел
        Node newNode = new Node(element);
        // если список пустой
        if (isEmpty()) {
            // начало и конец - это единственный узел
            last = newNode;
            first = newNode;
        } else {
            // следующий элемент после последнего - новый узел
            last.next = newNode;
            // теперь новый узел является последним
            last = newNode;
        }
        size++;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(T element) {
        // TODO: реализовать
        Node current = first;
        while (current != null) {
            if (current.value.equals(element)) {
                return true;
            } else {
                current = current.next; }
        }
        return false;
    }

    @Override
    public void remove(T element) {
        // TODO: реализовать
        Node current = first;
        while (current.next != null) {
            if (current.next.value.equals(element)) {
                current.next = current.next.next;
                System.out.println("Элемент удален " + element);
            } else {
                current = current.next;
            }
        }
        size--;
    }

    @Override
    public Iterator<T> iterator() {
        // TODO: реализовать
        return new LinkedListIterator();
    }

    private class LinkedListIterator<T> implements Iterator {

        private int currentValue = 0;
        private Node currentElement;

        @Override
        public boolean hasNext() {
            return currentValue < size;
        }

        @Override
        public T next() {
            if (!hasNext()) {
                return null;
            }
            return (T) currentElement.next;
        }
    }

    private boolean indexInBounds(int index) {
        return index >= 0 && index < size;
    }
}
