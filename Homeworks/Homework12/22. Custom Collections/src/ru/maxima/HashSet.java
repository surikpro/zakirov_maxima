package ru.maxima;

/**
 * 22.07.2021
 * 25. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class HashSet<T> implements Set<T> {

    private HashMap<T, Object> hashMap = new HashMap<>();

    private static final Object PRESENT = new Object();

    private int size = 0;

    @Override
    public void add(T element) {
        hashMap.put(element, PRESENT);
        size++;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(T element) {
        return hashMap.containsKey(element);
    }

    @Override
    public void remove(T element) {
        if (element != null) {
            HashMap.MapEntry<T, Object>[] entries = hashMap.getEntries();
            for (int i = 0; i < size - 1; i++) {
                if (entries[i].key.equals(element)) {
                    for (int j = i; j < size - 1; j++) {
                        entries[j].key = entries[j + 1].key;
                    }
                }
                System.out.println(entries[i].key);
            }
            size--;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new HashSetIterator();
    }
    private class HashSetIterator implements Iterator {

        private int current = 0;

        @Override
        public boolean hasNext() {
            return current < size;
        }

        @Override
        public T next() {
            if (!hasNext()) {
                return (T) hashMap.getEntries()[current++];
            }
            return  null;
        }
    }
}
