package ru.maxima;

/**
 * 08.07.2021
 * 22. Custom Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class BadSet<T> implements Set<T>  {

    BadMap<T, Object> map = new BadMap<>();

    private static final Object PRESENT = new Object();

    private int size = 0;

    @Override
    public void add(T element) {
        map.put(element, PRESENT);
        size++;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        if (size == 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean contains(T element) {
        return map.containsKey(element);
    }

    @Override
    public void remove(T element) {
        if (element != null) {
            BadMap.BadMapEntry entries[] = map.getEntries();
            for (int i = 0; i < size - 1; i++) {
                if (entries[i].key.equals(element)) {
                    for (int j = i; j < size - 1; j++) {
                        entries[j].key = entries[j + 1].key;
                    }
                }
                System.out.println(entries[i].key);
            }
            size--;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new BadSetIterator();
    }
    private class BadSetIterator<T> implements Iterator<T> {
        private int currentValue = 0;
        @Override
        public boolean hasNext() {
            return currentValue < size;
        }

        @Override
        public T next() {
            while (hasNext()) {
                return (T) map.getEntries()[currentValue++];
            }
            return null;
        }
    }
}
