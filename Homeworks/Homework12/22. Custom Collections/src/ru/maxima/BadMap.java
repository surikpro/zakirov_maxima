package ru.maxima;

/**
 * 08.07.2021
 * 22. Custom Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class BadMap<K, V> implements Map<K, V> {

    public static final int MAX_ARRAY_SIZE = 25;

    private BadMapEntry[] entries;

    protected static class BadMapEntry<K, V> implements Map.MapEntry<K, V> {
        K key;
        V value;

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }

        public BadMapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public K key() {
            return key;
        }

        @Override
        public V value() {
            return value;
        }
    }


    private int size;

    public BadMapEntry<K, V>[] getEntries() {
        return entries;
    }

    public BadMap() {
        this.entries = new BadMapEntry[MAX_ARRAY_SIZE];
    }

    @Override
    public void put(K key, V value) {
        for (int i = 0; i < size; i++) {
            // если уже есть значение с таким ключом
            if (entries[i].key.equals(key)) {
                // заменяем это значение
                entries[i].value = value;
                return;
            }
        }
        entries[size] = new BadMapEntry<>(key, value);
        size++;
    }

    @Override
    public V get(K key) {
        for (int i = 0; i < size; i++) {
            if (entries[i].key.equals(key)) {
                return (V) entries[i].value;
            }
        }
        return null;
    }

    @Override
    public Set<K> keySet() {
        // TODO: реализовать
        Set<K> keySet = new BadSet<>();
        for (int i = 0; i < size; i++) {
            keySet.add((K) entries[i].key);
        }
        return keySet;
    }

    @Override
    public Collection<V> values() {
        // TODO: реализовать
        Collection<V> collection = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            collection.add((V) entries[i].value);
        }
        return collection;
    }

    @Override
    public Set entrySet() {
        // TODO: реализовать
        Set<K> entrySet = new BadSet<>();
        for (int i = 0; i < size; i++) {
            K eachEntry = (K) (entries[i].key + " = " + entries[i].value);
            entrySet.add(eachEntry);
        }
        return entrySet;
    }

    @Override
    public boolean containsKey(K key) {
        for (int i = 0; i < size; i++) {
            if (entries[i].key.equals(key)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsValue(V value) {
        // TODO: реализовать
        for (int i = 0; i < size; i++) {
            if (entries[i].value.equals(value)) {
                return true;
            }
        }
        return false;
    }
}
