import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class ArrayWithRandomNumbers {
    //    1.1) Необходимо написать метод, возвращающий массив длиной N, N вводится с клавиатуры,
    //    массив должен быть заполнен рандомными числами. (использовать класс Random)
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Please enter the length of an array to proceed: ");
        int lengthOfArray = scanner.nextInt();
        System.out.println("Your requested array is: " + Arrays.toString(getArrayWithRandomNumbers(lengthOfArray)));

    }

    private static int[] getArrayWithRandomNumbers(int lengthOfArray) {
        Random randomNumber = new Random();
        int[] array = new int[lengthOfArray];
        for (int i = 0; i < lengthOfArray; i++) {
            array[i] = randomNumber.nextInt();
        }
        return array;
    }
}

