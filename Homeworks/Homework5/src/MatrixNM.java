import java.util.Arrays;
import java.util.Scanner;

public class MatrixNM {
    public static void main(String[] args) {
        int[][] matrix = getMatrixNM();
        System.out.println("Your array is " + Arrays.deepToString(matrix));
        System.out.println("Sum of integers is " + getSum(matrix));
    }

    private static int[][] getMatrixNM() {
        var scanner = new Scanner(System.in);
        System.out.print("Please enter the length of N: ");
        int matrixLengthN = scanner.nextInt();
        System.out.print("Please enter the length of M: ");
        int matrixLengthM = scanner.nextInt();
        int[][] matrix = new int[matrixLengthN][matrixLengthM];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                System.out.println("Please enter array elements. Current array is " + Arrays.deepToString(matrix));
                matrix[i][j] = scanner.nextInt();
            }
        }
        return matrix;
    }

    private static int getSum(int[][] matrix) {
        int sum = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (i == j) {
                    sum += matrix[i][j];
                }
            }
        }
        return sum;
    }
}
