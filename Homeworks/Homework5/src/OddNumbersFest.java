import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class OddNumbersFest {
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Please enter the length of an array: ");
        int lengthOfArray = scanner.nextInt();
//        System.out.println(Arrays.toString(getArrayWithRandomNumbers(lengthOfArray)));
        int[] array = getArrayWithRandomNumbers(lengthOfArray);
        System.out.println(Arrays.toString(array));
        System.out.print("Please enter the k number: ");
        int kNumber = scanner.nextInt();
        int[] new_array = addNumbers(array, kNumber);
        System.out.println(Arrays.toString(new_array));
        showOddNumbers(new_array);
    }

    private static int[] getArrayWithRandomNumbers(int lengthOfArray) {
        Random randomNumber = new Random();
        int[] array = new int[lengthOfArray];
        for (int i = 0; i < lengthOfArray; i++) {
            array[i] = randomNumber.nextInt();
        }
        return array;
    }

    private static int[] addNumbers(int[] array, int k) {
        int[] new_array = new int[array.length + k];
        int valueToK = 1;
        int j = 0;
        for (int i = 0; i < new_array.length; i++) {
            if (i < k) {
                new_array[i] = array[i];
                j++;
            } else {
                if (k > 0) {
                    new_array[i] = valueToK;
                    valueToK++;
                    k--;
                } else {
                    new_array[i] = array[j++];
                }
            }
        }
        return new_array;
    }

    private static void showOddNumbers(int[] new_array) {
        System.out.println("The odd numbers of the array are: ");
        for (int i = 0; i < new_array.length; i++) {
            if (new_array[i] % 2 != 0) {
                System.out.println(new_array[i]);
            }
        }
    }
}



