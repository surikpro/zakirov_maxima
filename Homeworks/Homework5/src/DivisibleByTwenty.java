import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class DivisibleByTwenty {
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Please enter the length of an array: ");
        int lengthOfArray = scanner.nextInt();
        int[] array = getArrayWithRandomNumbers(lengthOfArray);
        System.out.println("Your original array is " + Arrays.toString(array));
        System.out.print("Please enter k-numbers to delete from an array: ");
        int kNumber = scanner.nextInt();
        System.out.print("Please define the start position to delete the numbers: ");
        int lStartPosition = scanner.nextInt() - 1;
        int[] new_array = deleteKNumbersFromLPosition(array, kNumber, lStartPosition);
        System.out.println("Modified array is " + Arrays.toString(new_array));
        showDivisibleByTwentyElements(new_array);

    }

    private static int[] getArrayWithRandomNumbers(int lengthOfArray) {
        Random randomNumber = new Random();
        int[] array = new int[lengthOfArray];
        for (int i = 0; i < lengthOfArray; i++) {
            array[i] = randomNumber.nextInt();
        }
        return array;
    }
    private static int[] deleteKNumbersFromLPosition(int[] array, int kNumber, int lStartPosition) {
        int[] new_array = new int[array.length - kNumber]; // kNumber = 2;
        for (int i = 0; i < new_array.length; i++) {
            if (i < lStartPosition) {
                new_array[i] = array[i];
            } else {
                new_array[i] = array[i + kNumber]; // lPosition = 3
            }
        }
        return new_array;
    }
    private static void showDivisibleByTwentyElements(int[] new_array) {
        System.out.println("Elements of array divisible by 20 are: ");
        for (int i = 0; i < new_array.length; i++) {
            if (new_array[i] % 20 == 0) {
                System.out.println(new_array[i]);
            }
        }
    }
}

