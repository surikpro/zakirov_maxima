import java.util.Arrays;

public class ReverseThreeDigitNumber {
    public static void main(String[] args) {
        int[] array = {2, 301, 3, 345, 4, 5, 123};
        System.out.println("Original Array is " + Arrays.toString(array));
        int[] new_array = CopyArrayAndReverseAndAddThreeDigitNumbers(array);
        PrintArraysAndFirstDifferedElement(new_array, array);
    }

    private static int[] CopyArrayAndReverseAndAddThreeDigitNumbers(int[] array) {
        int[] new_array = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            new_array[i] = array[i];
            if ((new_array[i] >= 100) && (new_array[i] < 1000)) {
                int reversed = 0;
                while (new_array[i] != 0) {
                    int digit = new_array[i] % 10;
                    reversed = reversed * 10 + digit;
                    new_array[i] /= 10;
                }
                new_array[i] = array[i];
                new_array[i] = array[i] + reversed;
            }
        }
        return new_array;
    }

    private static void PrintArraysAndFirstDifferedElement(int[] new_array, int[] array) {
        System.out.println("Modified Array is " + Arrays.toString(new_array));
        int j = 1;
        for (int i = 0; i < array.length; i++) {
            if (new_array[i] != array[i]) {
                while (j < 2) {
                    System.out.println("The first differed elements are " + array[i] + " and " + new_array[i]);
                    j++;
                }
            }
        }
    }
}
