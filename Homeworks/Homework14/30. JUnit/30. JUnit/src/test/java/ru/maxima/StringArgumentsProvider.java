package ru.maxima;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

public class StringArgumentsProvider implements ArgumentsProvider {
    private Random random = new Random();
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
        List<Arguments> stringNumbers = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            String stringNumber = String.valueOf(random.nextInt(100));
            Arguments argument = Arguments.of(stringNumber);
            stringNumbers.add(argument);
        }
        return stringNumbers.stream();
    }
}
