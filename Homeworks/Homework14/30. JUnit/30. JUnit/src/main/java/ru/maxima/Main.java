package ru.maxima;

/**
 * 10.08.2021
 * 30. JUnit
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        NumbersUtil numbersUtil = new NumbersUtil();
        System.out.println(numbersUtil.gcd(64, 48));
        System.out.println(numbersUtil.parse("105.5"));
        System.out.println(numbersUtil.parse("-105.5"));
        System.out.println(numbersUtil.parse("abc"));
    }
}
