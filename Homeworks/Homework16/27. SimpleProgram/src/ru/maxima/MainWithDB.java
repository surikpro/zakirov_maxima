package ru.maxima;

import com.zaxxer.hikari.HikariDataSource;
import ru.maxima.dto.UserDto;
import ru.maxima.models.User;
import ru.maxima.repositories.UsersRepository;
import ru.maxima.repositories.UsersRepositoryDBBasedImpl;
import ru.maxima.repositories.UsersRepositoryFileBasedImpl;
import ru.maxima.services.UsersService;
import ru.maxima.util.IdGenerator;
import ru.maxima.util.IdGenerators;

import java.sql.*;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class MainWithDB {
    public static void main(String[] args) {
        UsersRepository usersRepository;
        UsersService usersService;

//        IdGenerator usersIdGenerator = IdGenerators.fileBasedGenerator("users_sequence.txt");
        try {
            Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/homework_db", "postgres", "qwerty007");
            usersRepository = new UsersRepositoryDBBasedImpl(connection);
            usersService = new UsersService(usersRepository);
        } catch (
                SQLException e) {
            throw new IllegalStateException(e);
        }
//        UsersRepository usersRepository = new UsersRepositoryFileBasedImpl("users.txt", usersIdGenerator);
//        UsersRepository usersRepository = new UsersRepositoryListImpl();

        Scanner scanner = new Scanner(System.in);

        Optional<User> currentOptional = Optional.empty();
        while (true) {
            System.out.println("1. Регистрация");
            System.out.println("2. Аутентификация");
            System.out.println("3. Список пользователей");
            System.out.println("4. Изменить пароль");
            System.out.println("5. Поиск по email в БД");

            int command = scanner.nextInt();
            scanner.nextLine();

            if (command == 1) {
                String email = scanner.nextLine();
                String password = scanner.nextLine();
                usersService.signUp(email, password);
            } else if (command == 2) {
                String email = scanner.nextLine();
                String password = scanner.nextLine();
                currentOptional = usersService.signIn(email, password);

                if (!currentOptional.isPresent()) {
                    System.err.println("Ошибка аутентификации");
                }
            } else if (command == 3) {
                if (!currentOptional.isPresent()) {
                    System.err.println("Вы не прошли аутентификацию");
                    continue;
                }
                List<UserDto> users = usersService.getUsers(currentOptional.get());
                System.out.println(users);
            } else if (command == 4) {
                if (currentOptional.isPresent()) {
                    System.out.println("Введите ваш новый email и пароль: ");
                    String email = scanner.nextLine();
                    String password = scanner.nextLine();
                    User user = currentOptional.get();
                    user.setEmail(email);
                    user.setPassword(password);
                    usersRepository.update(user);
                } else {
                    System.out.println("Такой пользователь не найден");
                    System.out.println("Пожалуйста, пройдите аутентификацию");
                }
            } else if (command == 5) {
                if (currentOptional.isPresent()) {
                    System.out.println("Пользователь аутентифицирован");
                    System.out.println("Введите email для поиска: ");
                    String emailForSearch = scanner.nextLine();
                    Optional<User> emailSearchUserOptional = usersService.findByEmail(emailForSearch);
                    if (emailSearchUserOptional.isPresent()) {
                        System.out.println("Email " + emailSearchUserOptional.get().getEmail() + " существует в БД");
                    } else {
                        System.out.println("Такого пользователя не существует");
                    }
                } else {
                    System.err.println("Пожалуйста, пройдите аутентификацию");
                }
            }
        }

    }
}
