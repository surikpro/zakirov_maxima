package ru.maxima.repositories;

import com.zaxxer.hikari.HikariDataSource;
import ru.maxima.models.User;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;
import java.util.Optional;

public class UsersRepositoryDBBasedImpl implements UsersRepository {
    private static final String SQL_SELECT_BY_EMAIL =
            "select userId, email, password from user_email_credentials where email = ?";
    private static final String SQL_UPDATE_BY_ID =
            "update user_email_credentials set email = ?, password = ? where userId = ?";
    Connection connection;

    public UsersRepositoryDBBasedImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void save(User user) {

    }

    @Override
    public Optional<User> findByEmail(String email) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_BY_EMAIL);
            preparedStatement.setString(1, email);
            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {
                if (result.getString("email").equals(email)) {
                    User user = new User(
                            result.getInt("userId"),
                            result.getString("email"),
                            result.getString("password")
                    );
                    return Optional.of(user);
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return Optional.empty();
    }

    @Override
    public void update(User user) {
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_BY_ID);) {
            statement.setString(1, user.getEmail());
            statement.setString(2, user.getPassword());
            statement.setInt(3, user.getId());
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Can't update");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public List<User> findAll() {
        return null;
    }
}
