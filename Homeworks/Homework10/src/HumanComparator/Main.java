package HumanComparator;

public class Main {
    public static void main(String[] args) {
        Human human1 = new Human("Aydar", 35, true);
        Human human2 = new Human("Aydar", 35, false);
        Human human3 = new Human("Aydar", 35, false);
        Human human4 = new Human("Aydar", 35, false);
        Human human5 = new Human("Aydar", 36, false);


        System.out.println("EqualsUtil1 - " + EqualsUtil.isEquals(human2, human3, human4));
        System.out.println("EqualsUtil2 - " + EqualsUtil.isEquals(human1, human2, human3, human5));

        System.out.println(human1);
        System.out.println(human2);
        System.out.println(human1.equals(human2)); // outcome is true: boolean true for human1 is not compared with boolean false of human2
        Animal bear = new Animal("bear", false);
        System.out.println(human1.equals(bear));

        System.out.println("HumanComparator.EqualsUtil 3 - " + EqualsUtil.isEquals("Hello", "My Dear", "What's up, man?"));
    }
}
