package HumanComparator;

public class Human {
    private String name;
    private int age;
    private boolean isMoving;

    public Human(String name, int age, boolean isMoving) {
        this.name = name;
        this.age = age;
        this.isMoving = isMoving;
    }
    @Override
    public String toString() {
        return "New HumanComparator.Human was created: " + "age - " + age + ", name - " + name;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Human)) {
            return false;
        }
        if (this == object) {
            return true;
        }

        Human that = (Human) object; // downcast
        return this.name == that.name && this.age == that.age;
    }
}
