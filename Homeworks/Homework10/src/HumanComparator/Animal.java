package HumanComparator;

public class Animal {
    private String type;
    private boolean isSwimming;

    public Animal(String type, boolean isSwimming) {
        this.type = type;
        this.isSwimming = isSwimming;
    }

    public String getType() {
        return type;
    }

    public boolean isSwimming() {
        return isSwimming;
    }
}
