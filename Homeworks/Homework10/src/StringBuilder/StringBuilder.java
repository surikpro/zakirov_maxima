package StringBuilder;

public class StringBuilder {
    private String stringValue;

    public StringBuilder() {
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    public String append(String newStringValue) {
        this.stringValue = this.stringValue + " " + newStringValue;
        return this.stringValue;
    }

    public String toString() {
        return "Our StringBuilder class is: " + getStringValue();
    }
}
