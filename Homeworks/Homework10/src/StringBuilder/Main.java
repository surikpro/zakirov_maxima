package StringBuilder;

public class Main {
    public static void main(String[] args) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.setStringValue("Hi!");
        System.out.println(stringBuilder.getStringValue());
        System.out.println(stringBuilder.append("What's up, man?"));
        System.out.println(stringBuilder.append("How are you doing?"));
        System.out.println(stringBuilder);

    }
}
