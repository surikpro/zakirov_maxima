package com.company.services;

import com.company.dto.AccountDto;
import com.company.repositories.AccountsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UsersService {

    private final AccountsRepository accountsRepository;
    @Override
    public List<AccountDto> getAllUsers() {
        return AccountDto.from(accountsRepository.findAll());
    }
}
