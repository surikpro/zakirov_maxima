package com.company.services;

import com.company.dto.SignUpForm;

public interface SignUpService {
    void signUp(SignUpForm form);
}
