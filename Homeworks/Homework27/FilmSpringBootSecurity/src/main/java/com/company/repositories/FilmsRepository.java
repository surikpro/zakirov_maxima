package com.company.repositories;

import com.company.models.Film;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FilmsRepository extends JpaRepository<Film, Long> {
    List<Film> findAll();
}
