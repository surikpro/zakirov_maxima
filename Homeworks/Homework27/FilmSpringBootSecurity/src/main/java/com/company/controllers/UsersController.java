package com.company.controllers;

import com.company.services.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequiredArgsConstructor
public class UsersController {

    private final UsersService usersService;

    @GetMapping("/users")
    public String getUsers(Model model) {
        model.addAttribute("accounts", usersService.getAllUsers());
        return "users";
    }
}
