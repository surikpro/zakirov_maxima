package com.company.services;

import com.company.dto.AccountDto;

import java.util.List;

public interface UsersService {
    List<AccountDto> getAllUsers();
}
