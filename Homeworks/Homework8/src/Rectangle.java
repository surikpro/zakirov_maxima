public class Rectangle extends GeometricalFigures {
    private double lengthOfSideA;
    private double lengthOfSideB;

    public Rectangle(String type, double lengthOfSideA, double lengthOfSideB) {
        super(type);
        this.lengthOfSideA = lengthOfSideA;
        this.lengthOfSideB = lengthOfSideB;
    }

    public double getArea() {
        return lengthOfSideA * lengthOfSideB;
    }

    @Override
    public double findPerimeter() {
        return lengthOfSideA * 2 + lengthOfSideB * 2;
    }
}
