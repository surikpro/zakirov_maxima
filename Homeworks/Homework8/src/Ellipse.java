public class Ellipse extends GeometricalFigures {
    private double radiusShort;
    private double radiusLong;

    public Ellipse(String type, double radiusShort, double radiusLong) {
        super(type);
        this.radiusShort = radiusShort;
        this.radiusLong = radiusLong;
    }

    public double getArea() {
        return Math.PI * radiusShort * radiusLong;
    }

    @Override
    public double findPerimeter() {
        return 2 * Math.PI * Math.sqrt((Math.pow(radiusShort, 2) + Math.pow(radiusLong, 2)) / 2);
    }
}
