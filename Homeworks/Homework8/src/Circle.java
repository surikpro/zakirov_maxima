public class Circle extends GeometricalFigures {
    private double radius;

    public Circle(String type, double radius) {
        super(type);
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public double findPerimeter() {
        return 2 * Math.PI * radius;
    }
}
