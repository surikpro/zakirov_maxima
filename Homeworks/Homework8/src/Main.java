public class Main {
    public static void main(String[] args) {
        Square square = new Square("square", 5.0);
        System.out.println("My object type is: " + square.getType());
        System.out.println("This is area of my object: " + square.getArea() + ". It can be centimeters or meters. Depends on your choice");
        System.out.println("The perimeter of my object is: " + square.findPerimeter());
        square.scale();
        square.move();

        Rectangle rectangle = new Rectangle("rectangle", 5.0, 6.0);
        System.out.println(rectangle.getType());
        System.out.println(rectangle.getArea());
        System.out.println(rectangle.findPerimeter());

        Circle circle = new Circle("circle", 25);
        System.out.println(circle.getType());
        System.out.println("The area of my object is: " + String.format("%.2f", circle.getArea()));
        System.out.println("The circumference of my object is " + String.format("%.1f", circle.findPerimeter()));

        Ellipse ellipse = new Ellipse("ellipse", 7.0, 9.0);
        System.out.println(ellipse.getType());
        System.out.println(String.format("%.3f", ellipse.getArea()));
        System.out.println(String.format("%.5f", ellipse.findPerimeter()));
    }
}
