public class Square extends GeometricalFigures implements Scalable, Movable {
    private double lengthOfSide;
    private final static int SCALE_X_BY = 5;
    private final static int MOVE_BY_METERS = 100;

    public Square(String type, double lengthOfSide) {
        super(type);
        this.lengthOfSide = lengthOfSide;
    }

    @Override
    public double getArea() {
        return lengthOfSide * lengthOfSide;
    }

    public double findPerimeter() {
        return lengthOfSide * 4;
    }

    @Override
    public void scale() {
        System.out.println("Now I can scale my objects by implementing interfaces. My scaled object is " + lengthOfSide * lengthOfSide * SCALE_X_BY);
    }

    public void move() {
        System.out.println("Yahoo! I am moving my objects by " + MOVE_BY_METERS + " meters!");
    }
}
