public abstract class GeometricalFigures {
    private String type;

    public GeometricalFigures(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public abstract double getArea();

    public abstract double findPerimeter();
}
