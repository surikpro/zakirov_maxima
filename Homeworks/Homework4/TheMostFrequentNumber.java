import java.util.Arrays;
import java.util.Scanner;

public class TheMostFrequentNumber {
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Please enter the size of an array: ");
        int count = scanner.nextInt();
        int[] array = createAnArray(count);
        System.out.println("Your array is " + Arrays.toString(array));
        System.out.println("The most frequent element in this array is " + findTheMostFrequentNumber(array, count));
//        Second option for creating array is:
//        int count = scanner.nextInt();
//        int[] array = new int[count];
    }
    private static int[] createAnArray(int count) {
        int[] array = new int[count];
        System.out.print("Please enter the number from -1000 to 1000: ");
        for (int i = 0; i < array.length; i++) {
            var scanner = new Scanner(System.in);
            array[i] = scanner.nextInt();
        }
        return array;
    }
    private static int findTheMostFrequentNumber(int[] array, int lengthOfArray) {
        Arrays.sort(array);

        int max_frequency_count = 1, valueOfTheElement = array[0];
        int current_count = 1;
        for (int i = 1; i < lengthOfArray; i++) {
            if (array[i] == array[i - 1])
                current_count++; // we increment value of current_count by 1 if values of nearby arrays are equal
            else {
                if (current_count > max_frequency_count) {
                    max_frequency_count = current_count;
                    valueOfTheElement = array[i - 1];
                }
                current_count = 1;
            }
        }
        if (current_count >= max_frequency_count) {
            max_frequency_count = current_count;
            valueOfTheElement = array[lengthOfArray -1];
        }
        return valueOfTheElement;
    }
}
