import java.util.Arrays;
import java.util.Scanner;

public class ParseInt {
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Please enter the size of an array: ");
        if (scanner.hasNext()) {
            int count = scanner.nextInt();
            int[] array = new int[count];
            System.out.print("Please enter the digits from null to nine: ");
            for (int i = 0; i < array.length; i++) {
                array[i] = scanner.nextInt();
                if ((array[i] < 0) || (array[i] > 10)) {
                    System.out.println("You last number cannot be validated. Please enter the digits from 0 to 9.");
                }
            }
            System.out.println("Your array is " + Arrays.toString(array) + ". Yahoo!");
            System.out.println("My Dear Friend! Your requested NUMBER is " + parse(array));
        } else {
            System.out.println("Sorry, You have NOT entered the NUMBER");
        }
    }
    private static int parse(int[] array) {
        int number = 0;
        for (int i = 0; i < array.length; i++) {
            number = number * 10 + array[i];
        }
        return number;
    }
}