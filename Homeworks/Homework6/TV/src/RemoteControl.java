import java.util.Random;

public class RemoteControl {
    private static RemoteControl instance;
    private String model;

    public static RemoteControl getInstance() {
        return instance;
    }
    static  {
        instance = new RemoteControl("Special_Remote_For_Samsung");
    }

    public RemoteControl(String model) {
        this.model = model;
        this.programmes = new Programme[5];
        this.programmesCount = 0;
    }

    public String getModel() {
        return model;
    }

    public Programme[] getProgrammes() {
        return this.programmes;
    }

    private Programme[] programmes;
    private int programmesCount;
    public Programme[] createAndReturnProgrammes(String programmeName) {
        Programme newProgramme = new Programme(programmeName);
        this.programmes[programmesCount] = newProgramme;
        programmesCount++;
        return this.programmes;
    }


    public void on(int channelNumber) {
        Random randomIndex = new Random();
        Programme randomProgramme = programmes[randomIndex.nextInt(5)];
        System.out.println(randomProgramme);

    }
}
