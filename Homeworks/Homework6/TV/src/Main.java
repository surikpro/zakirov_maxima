import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        TV samsung = new TV("Samsung");
        System.out.println("Special remote for our Samsung TV: " + samsung.createInstanceOfRemote("for_samsung"));
        System.out.println("The model of our remote is: " + RemoteControl.getInstance().getModel());
        Channel newChannel = new Channel("BBC", 1);
        System.out.println("Our channel name is: " + newChannel.getChannelName());
        RemoteControl.getInstance().createAndReturnProgrammes("A");
        RemoteControl.getInstance().createAndReturnProgrammes("B");
        System.out.println("Our list of programmes is: " + Arrays.toString(RemoteControl.getInstance().createAndReturnProgrammes("C")));
//        Programme[] programmes = RemoteControl.getInstance().getProgrammes();
        System.out.print("Our random programme for remote is: ");
        RemoteControl.getInstance().on(newChannel.getChannelNumber());

    }
}
