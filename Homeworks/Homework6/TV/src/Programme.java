public class Programme {
    private String programmeName;

    public Programme(String programmeName) {
        this.programmeName = programmeName;
    }

    public String getProgrammeName() {
        return programmeName;
    }
}
