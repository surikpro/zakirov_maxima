import java.util.Arrays;

public class TV {
    private String name;

    public TV(String name) {
        this.name = name;
        this.channels = new Channel[10];
        this.channelsCount = 0;
    }

    public String getName() {
        return name;
    }

    private Channel[] channels;
    private int channelsCount;
    public void createAndPrintChannel(String channelName, int channelNumber) {
        Channel newChannel = new Channel(channelName, channelNumber);
        this.channels[channelsCount] = newChannel;
        channelsCount++;
        System.out.println("String of channels is " + Arrays.deepToString(this.channels));
    }

    public RemoteControl createInstanceOfRemote(String model) {
        RemoteControl newRemote = RemoteControl.getInstance();
        return newRemote;
    }

}
