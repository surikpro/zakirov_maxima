import java.util.Arrays;

public class Channel {
    private String channelName;
    private int channelNumber;

    public Channel(String channelName, int channelNumber) {
        this.channelName = channelName;
        this.channelNumber = channelNumber;
    }

    public String getChannelName() {
        return channelName;
    }

    public int getChannelNumber() {
        return channelNumber;
    }
}