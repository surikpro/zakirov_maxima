package cars;

public class Car implements Comparable<Car>{
    String carNumber;
    String model;
    String colour;
    int mileage;
    int price;

    public Car(String carNumber, String model, String colour, int mileage, int price) {
        this.carNumber = carNumber;
        this.model = model;
        this.colour = colour;
        this.mileage = mileage;
        this.price = price;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public String getModel() {
        return model;
    }

    public String getColour() {
        return colour;
    }

    public int getMileage() {
        return mileage;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "cars.Car{" +
                "carNumber='" + carNumber + '\'' +
                ", model='" + model + '\'' +
                ", colour='" + colour + '\'' +
                ", mileage=" + mileage +
                ", price=" + price +
                '}';
    }

    @Override
    public int compareTo(Car o) {
        return this.price - o.price;
    }
}
