package cars;

import comparing.User;

import java.util.*;
import java.util.stream.Collectors;

public class MainForCar {
    private static List<Car> carsList() {
        Car car0 = new Car("aa008", "Toyota Camry", "Black", 0, 2500000);
        Car car1 = new Car("aa007у", "Nissan Qashqai", "White", 50000, 1200000);
        Car car2 = new Car("бб007с", "Porsche Cayenne", "White", 150000, 5000000);
        Car car3 = new Car("вв001я", "Nissan Micra", "Blue", 150000, 750000);
        Car car4 = new Car("дд002т", "Hyundai Solaris", "Red", 17000, 900000);
        Car car5 = new Car("сс003о", "Nissan Juke", "Black", 1, 1000000);
        Car car6 = new Car("во009о", "Toyota Rav4", "Black", 0, 760000);
        Car car7 = new Car("вы009у", "Toyota Rav4", "Black", 0, 780000);
        Car car8 = new Car("во009т", "Toyota Camry", "Black", 0, 2000000);
        Car car9 = new Car("во009т", "Toyota Camry", "Black", 0, 2000000);
        List<Car> cars = new ArrayList<>();
        cars.add(car0);
        cars.add(car1);
        cars.add(car2);
        cars.add(car3);
        cars.add(car4);
        cars.add(car5);
        cars.add(car6);
        cars.add(car7);
        cars.add(car8);
        cars.add(car9);
        return cars;
    }
    public static void main(String[] args) {
        System.out.println("All Black and Zero-mileage cars' numbers: ");
        carsList()
                .stream()
                .filter(car -> car.getMileage() == 0 || car.getColour().equals("Black"))
                .map(car -> car.carNumber)
                .forEach(System.out::println);

        System.out.println("_____________");
        System.out.println("Count of distinct models of cars with price 700000-800000: ");
        Long countOfDistinctModels = carsList()
                .stream()
                .filter(car -> car.getPrice() >= 700000 && car.getPrice() <= 800000)
                .map(car -> car.getModel())
                .distinct()
                .count();
        System.out.println(countOfDistinctModels);

        System.out.println("_____________");
        System.out.println("Colour of the car with the minimum price: ");
        String colourOfCarMinPrice = carsList()
                .stream()
                .min(Car::compareTo)
                .map(car -> car.getColour())
                .get();
        System.out.println(colourOfCarMinPrice);

        System.out.println("_____________");
        System.out.println("Minimum price of the car from the list: ");
        System.out.println(carsList()
                .stream()
                .min(Car::compareTo)
                .map(car -> car.getPrice())
                .get());
        System.out.println("_____________");
        System.out.println("Average price of all cars is: ");
        OptionalDouble sum = carsList()
                .stream()
                .mapToInt(Car::getPrice)
                .average();
        System.out.println((int)sum.getAsDouble());

        System.out.println("_____________");
        System.out.println("Average price of all Toyota Camry cars is: ");
        OptionalDouble sumOfAllCamryCars = carsList()
                .stream()
                .filter(car -> car.getModel().equals("Toyota Camry"))
                .mapToInt(Car::getPrice)
                .average();
        System.out.println((int)sumOfAllCamryCars.getAsDouble());

    }
}
