package files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class Main {
    public static void main(String[] args) {
        try {
            File file = new File("C:\\Users\\aydar\\Desktop\\lessons-maxima\\Stream API exercise\\Stream API exercise project\\input.txt");
            BufferedReader reader = new BufferedReader(new FileReader(file));
            reader.lines().forEach(System.out::println);
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }
}
