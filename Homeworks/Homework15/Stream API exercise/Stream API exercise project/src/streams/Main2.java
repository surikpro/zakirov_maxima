package streams;

import comparing.User;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Main2 {
    public static void main(String[] args) {
        User user0 = new User(40L, "Марсель", "Сидиков", 28, 1.85);
        User user1 = new User(70L, "Алия", "Мухутдинова", 20, 1.71);
        User user2 = new User(13L, "Айрат", "Мухутдинов", 25, 1.54);
        User user3 = new User(44L, "Даниил", "Вдовинов", 29, 1.65);
        User user4 = new User(1L, "Салават", "Забиров", 22, 1.67);
        User user5 = new User(15L, "Ильгам", "Хасанов", 21, 1.75);
        User user6 = new User(9L, "Артур", "Колычев", 20, 1.69);


        List<User> users = new ArrayList<>();
        users.add(user0);
        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
        users.add(user5);
        users.add(user6);
        Stream<User> userStream = users.stream();
        Predicate<User> ageMoreThan22 = user -> user.getAge() > 22;
        Comparator<User> userByAgeComparator = (o1, o2) -> o1.getAge() - o2.getAge();
        Function<User, String> getNameOfUser = user -> user.getFirstName() + " " + user.getLastName();
        Consumer<User> printUser = user -> System.out.println(user);
        Consumer<String> printName = name -> System.out.println(name);
        userStream
                .filter(ageMoreThan22)
                .sorted(userByAgeComparator)
                .map(getNameOfUser)
                .forEach(printName);

//        filteredStream.forEach(printUser);
//        sortedStream.forEach(printUser);
    }
}
