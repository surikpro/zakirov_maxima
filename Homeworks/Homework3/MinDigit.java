import java.util.Scanner;

public class MinDigit {
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.println("Please enter any natural number: ");
        int number = scanner.nextInt();
        System.out.println("The smallest digit of number " + number + " is " + minDigit(number));
    }
    public static int minDigit(int number) {
        int smallestDigit = number % 10;
        int remainder = 0;
        while (number != 0) {
            remainder = number % 10;
            if (smallestDigit > remainder) {
                smallestDigit = remainder;
            }
            number /= 10;
        }
        return smallestDigit;
    }
}

