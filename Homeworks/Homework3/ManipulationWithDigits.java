import java.util.Scanner;

public class ManipulationWithDigits {
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.println("Please enter a number: ");
        int currentNumber = scanner.nextInt();

        while (currentNumber != -1) {
            if (isEven(currentNumber) == true) {
                System.out.println("The minimum digit of a number " + currentNumber + " is " + minDigit(currentNumber));
            }
            else {
                System.out.println("The average of digits of a number " + currentNumber + " is " + getDigitsAverage(currentNumber));
            }
            System.out.println("Please enter a new number");
            currentNumber = scanner.nextInt();
        }
    }

    public static boolean isEven(int currentNumber) {
        if (currentNumber % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }

    public static int minDigit(int currentNumber) {
        int smallestDigit = currentNumber % 10;
        int remainder = 0;
        while (currentNumber != 0) {
            remainder = currentNumber % 10;
            if (smallestDigit > remainder) {
                smallestDigit = remainder;
            }
            currentNumber /= 10;
        }
        return smallestDigit;
    }

    public static double getDigitsAverage(int currentNumber) {
        double sum = 0;
        double numberOfDigits = 0;
        double averageOfDigits = 0;
        while (currentNumber > 0) {
            numberOfDigits++;
            sum += currentNumber % 10;
            currentNumber /= 10;
            averageOfDigits = sum / numberOfDigits;
        }
        return averageOfDigits;
    }
}
