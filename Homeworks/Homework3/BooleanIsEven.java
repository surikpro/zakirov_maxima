import java.util.Scanner;

public class BooleanIsEven {
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.println("Please enter any number to check: ");
        int number = scanner.nextInt();
        System.out.println(isEven(number));
    }
    public static boolean isEven(int number) {
        if (number % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }
}