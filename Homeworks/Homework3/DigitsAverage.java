import java.util.Scanner;

public class DigitsAverage {
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.println("Please enter any number to get average: ");
        int number = scanner.nextInt();
        System.out.println("The average of digits of a given number is " + getDigitsAverage(number));
    }
    public static double getDigitsAverage(int number) {
        double sum = 0;
        double numberOfDigits = 0;
        double averageOfDigits = 0;
        while (number > 0) {
            numberOfDigits++;
            sum += number % 10; // int type here implicitly becomes double
            number /= 10;
            averageOfDigits = sum / numberOfDigits;
        }
         return averageOfDigits;
    }
}