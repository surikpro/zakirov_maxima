import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] numbers = {345, 505078, 605};
        String[] stringElements = {"Summer1", "Winter", "Spring"};
        NumbersAndStringProcessor processor = new NumbersAndStringProcessor(numbers, stringElements);
//        processor.setNumbers(numbers);
        NumbersProcess process = (number) -> {
            int reverse = 0;
            while (number != 0) {
                int digit = number % 10;
                if (digit != 0) {
                    if (digit % 2 != 0) {
                        reverse += digit - 1;
                    } else {
                        reverse += digit;
                    }
                    reverse = reverse * 10;
                }
                number /= 10;
            }
//            System.out.println(reverse/10);
            return reverse / 10;
        };
        System.out.println(Arrays.toString(processor.getNumbers()));
        System.out.println(Arrays.toString(processor.process(process)));

        StringsProcess processStrings = (processedStr) -> {
            char[] array = processedStr.toCharArray();
            String output = "";
            for (int i = array.length - 1; i >= 0; i--) {
                output += array[i];
            }
            return output.toUpperCase().replaceAll("\\d", "");
        };
        System.out.println(Arrays.toString(processor.getStringSequence()));
        System.out.println(Arrays.toString(processor.process(processStrings)));
    }
}


