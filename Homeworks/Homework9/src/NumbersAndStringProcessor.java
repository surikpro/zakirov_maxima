import java.util.Arrays;

public class NumbersAndStringProcessor {
    private int[] numbers;
    private String[] stringSequence;

    public NumbersAndStringProcessor(int[] numbers, String[] stringSequence) {
        this.stringSequence = stringSequence;
        this.numbers = numbers;
    }

    public void setStringSequence(String[] stringSequence) {
        this.stringSequence = stringSequence;
    }

    public void setNumbers(int[] numbers) {
        this.numbers = numbers;
    }

    public String[] getStringSequence() {
        return stringSequence;
    }

    public int[] getNumbers() {
        return numbers;
    }

    public int[] process(NumbersProcess numbersProcess) {
        int[] newElements = new int[getNumbers().length];
        for (int i = 0; i < getNumbers().length; i++) {
            int processed = numbersProcess.process(numbers[i]);
            newElements[i] = processed;
        }
        return newElements;
    }
    public String[] process(StringsProcess stringsProcess) {
        String[] newString = new String[getStringSequence().length];
        for (int i = 0; i < getStringSequence().length; i++) {
            String processedStr = stringsProcess.process(stringSequence[i]);
            newString[i] = processedStr;
        }
        return newString;
    }
}
