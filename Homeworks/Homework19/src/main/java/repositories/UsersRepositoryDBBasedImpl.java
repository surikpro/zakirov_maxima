package repositories;

import models.User;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UsersRepositoryDBBasedImpl implements UsersRepository{
    //language=SQL
    private static final String SQL_SELECT_ALL =
            "select * from user_email_credentials order by userId";
    private static final String SQL_SELECT_BY_EMAIL =
            "select userId, email, password from user_email_credentials where email = ?";
    //language=SQL
    private static final String SQL_UPDATE_BY_EMAIL =
            "update user_email_credentials set email = ?, password = ? where userId = ?;";
    //language=SQL
    private static final String SQL_INSERT =
            "insert into user_email_credentials(email, password) values (?, ?) RETURNING userId";
    //language=SQL

    private JdbcTemplate jdbcTemplate;

    private final RowMapper<User> userRowMapper = (row, rowNumber) -> new User(
            row.getInt("userId"),
            row.getString("email"),
            row.getString("password"));

    public UsersRepositoryDBBasedImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public void save(User user) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement preparedStatement =
                    connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, user.getEmail());
            preparedStatement.setString(2, user.getPassword());
            return preparedStatement;
        }, keyHolder);
        user.setId(keyHolder.getKey().intValue());
    }

    @Override
    public Optional<User> findByEmail(String email) {
        try {
            return Optional.of(jdbcTemplate.queryForObject(SQL_SELECT_BY_EMAIL, userRowMapper, email));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }

    }

    @Override
    public List<User> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, userRowMapper);
    }

    @Override
    public void update(User user) {
        jdbcTemplate.update(SQL_UPDATE_BY_EMAIL,
        user.getEmail(),
        user.getPassword(),
        user.getId());
    }
}
