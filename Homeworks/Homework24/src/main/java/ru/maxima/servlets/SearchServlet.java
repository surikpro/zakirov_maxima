package ru.maxima.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import ru.maxima.dto.ProductDto;
import ru.maxima.dto.ProductsResponseDto;
import ru.maxima.services.ProductsService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 23.09.2021
 * 42. Java Servlet JSP
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@WebServlet("/searchByProductName")
public class SearchServlet extends HttpServlet {

    private ProductsService productsService;
    private ObjectMapper objectMapper;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute("springContext");
        this.productsService = applicationContext.getBean(ProductsService.class);
        this.objectMapper = applicationContext.getBean(ObjectMapper.class);
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("name") == null) {
            request.getRequestDispatcher("/jsp/search_page.jsp").forward(request, response);
        } else {
            List<ProductDto> products = productsService.searchProductByName(request.getParameter("name"));
            String jsonResponse = objectMapper.writeValueAsString(new ProductsResponseDto(products));
            response.setStatus(200);
            response.setContentType("application/json");
            response.getWriter().println(jsonResponse);
        }
    }
}
