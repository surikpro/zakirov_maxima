package ru.maxima.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * 30.09.2021
 * 42. Java Servlet JSP
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@WebFilter("/*")
public class AuthenticationFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        if (isAuthenticated(request)) {
            System.out.println("Аутентифицирован");
        } else {
            System.out.println("Не аутентифицирован");
        }
        chain.doFilter(request, response);
    }

    private boolean isAuthenticated(HttpServletRequest request) {
        Boolean result = (Boolean) request.getSession().getAttribute("isAuthenticated");
        return result != null && result;
    }
    @Override
    public void destroy() {

    }
}
