package ru.maxima.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 30.09.2021
 * 42. Java Servlet JSP
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@WebFilter("/*")
public class ColorFilter implements Filter {

    private static final String COLOR_COOKIE_NAME = "profilePageColor";
    private static final String COLOR_PARAMETER_NAME = "color";
    private static final String COLOR_ATTRIBUTE_NAME = "color";
    private static final int COLOR_COOKIE_MAX_AGE = 60 * 60 * 24 * 365;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpServletResponse response = (HttpServletResponse)servletResponse;

        String color = request.getParameter(COLOR_PARAMETER_NAME);

        if (color != null) {
            createColorCookie(response, color);
        } else {
            color = processCookie(request);
        }

        request.setAttribute(COLOR_ATTRIBUTE_NAME, color);
        chain.doFilter(request, response);
    }

    private String processCookie(HttpServletRequest request) {
        String color = null;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(COLOR_COOKIE_NAME)) {
                    color = cookie.getValue();
                    break;
                }
            }
        }
        return color;
    }

    private void createColorCookie(HttpServletResponse response, String color) {
        Cookie cookie = new Cookie(COLOR_COOKIE_NAME, color);
        cookie.setMaxAge(COLOR_COOKIE_MAX_AGE);
        response.addCookie(cookie);
    }

    @Override
    public void destroy() {

    }
}
