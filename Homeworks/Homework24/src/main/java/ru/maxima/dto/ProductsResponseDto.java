package ru.maxima.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@AllArgsConstructor
@Data
public class ProductsResponseDto {
    private List<ProductDto> products;
}
