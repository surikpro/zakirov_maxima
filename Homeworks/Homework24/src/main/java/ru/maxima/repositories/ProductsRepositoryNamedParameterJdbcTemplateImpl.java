package ru.maxima.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.maxima.models.Product;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.List;

/**
 * 20.08.2021
 * 33. Simple Program with JDBC
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Repository
public class ProductsRepositoryNamedParameterJdbcTemplateImpl implements ProductsRepository {

    //language=SQL
    private static final String SQL_INSERT =
            "insert into product(name) values (:name) RETURNING id";

    //language=SQL
    private static final String SQL_SELECT_ALL =
            "select * from product order by id";

    //language=SQL
    private static final String SQL_SELECT_ALL_BY_NAME_LIKE =
            "select * from product where name ilike :name";

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private static final RowMapper<Product> productRowMapper = (row, rowNumber) -> Product.builder()
            .id(row.getInt("id"))
            .name(row.getString("name"))
            .build();

    @Autowired
    public ProductsRepositoryNamedParameterJdbcTemplateImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public void save(Product product) {
        // данный объект запоминает сгенерированные базой данных ключи
        KeyHolder keyHolder = new GeneratedKeyHolder();
        namedParameterJdbcTemplate.update(SQL_INSERT, (new MapSqlParameterSource()
                .addValue("name", product.getName())), keyHolder, new String[]{"id"});
        product.setId(keyHolder.getKey().intValue());
    }

    @Override
    public List<Product> findAll() {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
    }

    @Override
    public List<Product> searchByName(String name) {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_BY_NAME_LIKE,
                Collections.singletonMap("name", "%" + name + "%"),
                productRowMapper);
    }
}
