package ru.maxima.services;

import org.springframework.stereotype.Service;
import ru.maxima.dto.ProductDto;
import ru.maxima.models.Product;
import ru.maxima.repositories.ProductsRepository;

import java.util.List;
import java.util.stream.Collectors;

import static ru.maxima.dto.ProductDto.from;

@Service
public class ProductsServiceImpl implements ProductsService{
    private final ProductsRepository productsRepository;

    public ProductsServiceImpl(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    @Override
    public void signUp(String name) {
        Product product = Product.builder()
                .name(name)
                .build();

        productsRepository.save(product);
    }

    @Override
    public List<String> getAllNames() {
        return productsRepository.findAll().stream().map(Product::getName).collect(Collectors.toList());
    }

    @Override
    public List<ProductDto> getAllProducts() {
        return from(productsRepository.findAll());
    }

    @Override
    public List<ProductDto> searchProductByName(String name) {
        return ProductDto.from(productsRepository.searchByName(name));
    }
}
