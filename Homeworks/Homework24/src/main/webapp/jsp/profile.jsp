<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
<h1 style="color: ${color}">Profile Page</h1>
<form>
  <input type="radio" id="redColor" name="color" value="red">
  <label for="redColor">Red</label>
  <input type="radio" id="blueColor" name="color" value="blue">
  <label for="blueColor">Blue</label>
  <input type="radio" id="greenColor" name="color" value="green">
  <label for="greenColor">Green</label>
  <input type="submit" value="Save color">
</form>
</body>
</html>