<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Products Search Page</title>
</head>
<script>
    function searchProducts(name) {
        let request = new XMLHttpRequest();

        request.open('GET', '/searchByProductName?name=' + name, false);

        request.send();

        if (request.status !== 200) {
            alert("Ошибка!")
        } else {
            let html = '<tr>' +
                '<th>Id</th>' +
                '<th>Product</th>' +
                '</tr>';


            let response = JSON.parse(request.response);

            for (let i = 0; i < response['products'].length; i++) {
                html += '<tr>';
                html += '<td>' + response['products'][i]['id'] + '</td>';
                html += '<td>' + response['products'][i]['name'] + '</td>';
                html += '</tr>'
            }

            document.getElementById('product_table').innerHTML = html;
        }
    }
</script>
<body>
<h1 style="color: ${color}">Products Search Page</h1>
<label for="name">Enter product for search</label>
<input id="name" name="name" placeholder="Product" onkeyup="searchProducts(document.getElementById('name').value)">
<br>

<%--<table>--%>
<%--    <tr>--%>
<%--        <th>Id</th>--%>
<%--        <th>Product Name</th>--%>
<%--    </tr>--%>

<%--    <c:forEach items="${products}" var="product">--%>
<%--        <tr>--%>
<%--            <td>${product.id}</td>--%>
<%--            <td>${product.productName}</td>--%>
<%--        </tr>--%>
<%--    </c:forEach>--%>
<%--</table>--%>

<table id="product_table">

</table>
</body>
</html>
