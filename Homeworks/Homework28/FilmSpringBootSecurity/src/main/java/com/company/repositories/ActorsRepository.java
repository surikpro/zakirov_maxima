package com.company.repositories;

import com.company.models.Actor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**

 */

public interface ActorsRepository extends JpaRepository<Actor, Long> {
    List<Actor> findAllByFilm_Id(Long filmId);
    List<Actor> findAll();
    List<Actor> findAllByFilmIsNull();

}
