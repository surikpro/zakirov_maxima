package com.company.controllers;

import com.company.services.FilesService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**

 */
@RequiredArgsConstructor
@Controller
@RequestMapping("/files")
public class FilesController {

    private final FilesService filesService;

    @GetMapping("/upload")
    public String getFilesUploadPage() {
        return "file_upload_page";
    }

    @PostMapping("/upload")
    public String uploadFile(@RequestParam("file")  MultipartFile file, @RequestParam("description") String description) {
        filesService.saveFile(file, description);
        return "file_upload_page";
    }

    @GetMapping("/{file-name:.+}")
    public void getFile(@PathVariable("file-name") String fileName, HttpServletResponse response) {
        filesService.addFileToResponse(fileName, response);
    }
}
