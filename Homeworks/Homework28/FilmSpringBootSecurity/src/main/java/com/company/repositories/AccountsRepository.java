package com.company.repositories;

import com.company.models.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AccountsRepository extends JpaRepository<Account, Long> {
    Optional<Account> findByEmail(String email);

    List<Account> findAllByState(Account.State state);

    List<Account> findByEmailLike(String email);

    Optional<Account> findByConfirmUUID(String uuid);
}
