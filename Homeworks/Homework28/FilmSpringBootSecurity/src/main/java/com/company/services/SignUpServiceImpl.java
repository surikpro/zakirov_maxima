package com.company.services;

import com.company.dto.SignUpForm;
import com.company.models.Account;
import com.company.repositories.AccountsRepository;
import com.company.util.MailUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class SignUpServiceImpl implements SignUpService {

    private final PasswordEncoder passwordEncoder;
    private final AccountsRepository accountsRepository;
    private final MailUtil mailUtil;

    @Override
    public void signUp(SignUpForm form) {
        Account account = Account.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .email(form.getEmail())
                .hashPassword(passwordEncoder.encode(form.getPassword()))
                .role(Account.Role.USER)
                .state(Account.State.NOT_CONFIRMED)
                .confirmUUID(UUID.randomUUID().toString())
                .build();
        mailUtil.sendMailForConfirm(account.getFirstName(), account.getEmail(), account.getConfirmUUID());
        accountsRepository.save(account);
    }
}