package com.company.security.details;

import com.company.models.Account;
import com.company.repositories.AccountsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service("customUserDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    private final AccountsRepository accountsRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        Account account = accountsRepository.findByEmail(email).orElseThrow(() ->
                new UsernameNotFoundException("User not found"));

        return new UserDetailsImpl(account);
    }
}
