package com.company.services;

import com.company.dto.ActorDto;
import com.company.models.Actor;
import com.company.models.FileInfo;
import com.company.models.Film;
import com.company.repositories.FilesInfoRepository;
import com.company.repositories.FilmsRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

/**

 */
@RequiredArgsConstructor
@Service
public class FilesServiceImpl implements FilesService {

    private final FilesInfoRepository filesInfoRepository;
    private final FilmsRepository filmsRepository;

    @Value("${storage.folder}")
    private String storageFolder;

    @Override
    public void saveFile(MultipartFile file, String description) {
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        FileInfo fileInfo = FileInfo.builder()
                .description(description)
                .mimeType(file.getContentType())
                .originalFileName(file.getOriginalFilename())
                .storageFileName(UUID.randomUUID() + "." + extension)
                .size(file.getSize())
                .build();

        filesInfoRepository.save(fileInfo);

        try {
            Files.copy(file.getInputStream(), Paths.get(storageFolder, fileInfo.getStorageFileName()));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }


    }

    @Override
    public Object addFileToResponse(String fileName, HttpServletResponse response) {
        FileInfo fileInfo = filesInfoRepository.findByStorageFileName(fileName);
        response.setContentType(fileInfo.getMimeType());
        response.setContentLength(fileInfo.getSize().intValue());
        response.setHeader("Content-Disposition", "filename=\"" + fileInfo.getOriginalFileName() + "\"");
        try {
            IOUtils.copy(new FileInputStream(storageFolder + "/" + fileName), response.getOutputStream());
            response.flushBuffer();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return null;
    }

    @Override
    public List<FileInfo> getAllById(Long filmId) {
        return filesInfoRepository.findAllByFilm_Id(filmId);
    }

    @Override
    public void saveFileByFilmId(MultipartFile file, String description, Long filmId) {
        Film film = filmsRepository.getById(filmId);
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        FileInfo fileInfo = FileInfo.builder()
                .description(description)
                .mimeType(file.getContentType())
                .originalFileName(file.getOriginalFilename())
                .storageFileName(UUID.randomUUID() + "." + extension)
                .size(file.getSize())
                .film(film)
                .build();

        filesInfoRepository.save(fileInfo);
        try {
            Files.copy(file.getInputStream(), Paths.get(storageFolder, fileInfo.getStorageFileName()));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
