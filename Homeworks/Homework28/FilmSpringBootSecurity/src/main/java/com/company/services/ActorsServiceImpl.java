package com.company.services;

import com.company.dto.ActorDto;
import com.company.dto.AddActorForm;
import com.company.models.Actor;
import com.company.repositories.ActorsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.company.dto.ActorDto.from;


/**

 */
@Service
@RequiredArgsConstructor
public class ActorsServiceImpl implements ActorsService {

    private final ActorsRepository actorsRepository;

    @Override
    public List<ActorDto> getAllActors() {
        return from(actorsRepository.findAll());
    }

    @Override
    public void addActor(AddActorForm form) {
        Actor actor = Actor.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .build();

        actorsRepository.save(actor);
    }
}
