package com.company.controllers;

import com.company.dto.ActorDto;
import com.company.dto.FilmDto;
import com.company.services.FilesService;
import com.company.services.FilmsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class FilmsController {

    private final FilmsService filmsService;
    private final FilesService filesService;

    @RequestMapping("/films")
    public String getFilms(Model model) {
        model.addAttribute("films", filmsService.getAllFilms());
        return "films";
    }

    @RequestMapping(value = "/films", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public List<FilmDto> addFilm(@RequestBody FilmDto film) {
        filmsService.addFilm(film);
        return filmsService.getAllFilms();
    }

    @RequestMapping("/films/{film-id}/profile")
    public String getActorsOfFilmProfile(@PathVariable("film-id") Long filmId, Model model) {
        model.addAttribute("film", filmsService.getFilmById(filmId));
        model.addAttribute("actors", filmsService.getActorsByFilm(filmId));
        model.addAttribute("files", filesService.getAllById(filmId));
        return "film_profile";
    }

    @RequestMapping("/films/{film-id}/actors")
    public String getActorsOfFilmPage(@PathVariable("film-id") Long filmId, Model model) {
        model.addAttribute("film", filmsService.getFilmById(filmId));
        model.addAttribute("actors", filmsService.getActorsByFilm(filmId));
        model.addAttribute("actorsWithoutFilm", filmsService.getActorsWithoutFilm());
        return "actors_of_film";
    }

    @RequestMapping(value = "/films/{film-id}/actors", method = RequestMethod.POST)
    public String addActorToFilm(@PathVariable("film-id") Long filmId, ActorDto actor) {
        filmsService.addActorToFilm(filmId, actor);
        return "redirect:/films/" + filmId + "/actors";
    }

    @GetMapping("/films/{film-id}")
    public String getFilmById(@PathVariable("film-id") Long filmId, Model model) {
        model.addAttribute("film", filmsService.getFilmById(filmId));
        return "film_id_page";
    }

    @PostMapping("/films/{film-id}/profile")
    public String uploadFile(@PathVariable("film-id") Long filmId, @RequestParam("file") MultipartFile file, @RequestParam("description") String description) {
        filesService.saveFileByFilmId(file, description, filmId);
        return "redirect:/films/" + filmId + "/profile";
    }

}
