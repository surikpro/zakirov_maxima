package com.company.controllers;

import com.company.dto.AddActorForm;
import com.company.services.ActorsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 *
 */
@Controller
@RequiredArgsConstructor
public class ActorsController {

    private final ActorsService actorsService;

    @RequestMapping("/actors")
    public String getActorsPage(Model model) {
        model.addAttribute("actors", actorsService.getAllActors());
        return "actors";
    }

    @RequestMapping(value = "/actors", method = RequestMethod.POST)
    public String addActor(AddActorForm form) {
        actorsService.addActor(form);
        return "redirect:/actors";
    }

}
