package com.company.dto;

import com.company.models.Actor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

/**

 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ActorDto {
    private Long id;
    private String firstName;
    private String lastName;

    public static ActorDto from(Actor actor) {
        return ActorDto.builder()
                .id(actor.getId())
                .firstName(actor.getFirstName())
                .lastName(actor.getLastName())
                .build();
    }

    public static List<ActorDto> from(List<Actor> actors) {
        return actors.stream().map(ActorDto::from).collect(Collectors.toList());
    }
}
