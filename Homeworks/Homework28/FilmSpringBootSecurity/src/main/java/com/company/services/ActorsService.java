package com.company.services;

import com.company.dto.ActorDto;
import com.company.dto.AddActorForm;

import java.util.List;

/**

 */
public interface ActorsService {
    void addActor(AddActorForm form);
    List<ActorDto> getAllActors();
}
