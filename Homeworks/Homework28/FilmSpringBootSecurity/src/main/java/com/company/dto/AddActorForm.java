package com.company.dto;

import lombok.Data;

/**

 */
@Data
public class AddActorForm {
    private String firstName;
    private String lastName;
}
