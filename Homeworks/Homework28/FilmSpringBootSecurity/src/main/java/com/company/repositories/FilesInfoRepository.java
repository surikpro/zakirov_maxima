package com.company.repositories;

import com.company.models.Actor;
import org.springframework.data.jpa.repository.JpaRepository;
import com.company.models.FileInfo;

import java.util.List;

/**

 */
public interface FilesInfoRepository extends JpaRepository<FileInfo, Long> {
    FileInfo findByStorageFileName(String fileName);
    List<FileInfo> findAll();

    List<FileInfo> findAllByFilm_Id(Long filmId);
}
