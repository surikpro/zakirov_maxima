const express = require('express');
const app = express();
const bodyParser = require('body-parser').json();
const fileStorage = require('fs');

require('./app/routes')(app, bodyParser, fileStorage);

app.use(express.static('public'));
app.listen(8080);
console.log('Server started at http://localhost:8080');