const usersRoutes = require('./users_routes');
const bodyParser = require("body-parser");

module.exports = function (app, bodyParser, fileStorage) {
    usersRoutes(app, bodyParser, fileStorage);
}