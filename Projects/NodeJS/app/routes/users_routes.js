module.exports = function (app, bodyParser, fileStorage) {
    app.post('/users', bodyParser, function (request, response) {
        let body = request.body;
        fileStorage.appendFile('storage/storage.txt', body.firstName + ' ' + body.lastName + '\n',
            function (error) {
                if (error) {
                    throw error;
                } else {
                    console.log('Info was saved!')
                    response.redirect('/');
                }
            })
    });
    app.get('/users', function (request, response) {
        fileStorage.readFile('storage/storage.txt', 'utf-8', function (error, data) {
            let lines = data.split('\n');
            let responseBody = [];
            for (let i = 0; i < lines.length; i++) {
                responseBody.push(
                    {
                        'firstName': lines[i].split(' ')[0],
                        'lastName': lines[i].split(' ')[1]
                    }
                )
            }
            response.setHeader('Content-Type', 'application/json');
            response.send(JSON.stringify(responseBody));
        })
        });
}